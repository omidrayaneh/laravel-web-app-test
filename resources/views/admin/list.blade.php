@extends('layouts.master.master')

@section('content')
    <div class="content-wrapper" style="min-height: 511px;">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <h1>Laravel Test Contact List Page</h1>
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- /.store message -->
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
    <!-- /.store message -->

        <!-- Main content -->
        <section>
            <section class="content">
                <div class="container-fluid" id="app">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th >#</th>
                            <th >Name</th>
                            <th >Email</th>
                            <th >Message</th>
                            <th >Created At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $message)
                        <tr>
                            <th >{{$message->id}}</th>
                            <td >{{$message->name}}</td>
                            <td>{{$message->email}}</td>
                            <td>{{$message->message}}</td>
                            <td>{{$message->created_at}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </section>
        </section>
        <!-- /.content -->
    </div>
@endsection
