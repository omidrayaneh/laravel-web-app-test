@extends('layouts.master.master')

@section('content')
    <div class="content-wrapper" style="min-height: 511px;">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <h1>Laravel Test Contact Page</h1>
                </div>
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section>
            <section class="content">
                <div class="container-fluid" id="app">
                    <form action="{{route('store.message')}}" method="post">
                        @csrf
                        <div class="col-md-4">
                            <label for="">Name :</label> <input value="{{old('name')}}" name="name" placeholder="Name" class=" form-control" type="text">
                            <small class="text-danger">@error('name') {{$message}}@enderror</small>
                        </div>
                        <div class="col-md-4">
                            <label for=""> Email :</label> <input value="{{old('email')}}" name="email" placeholder="Email" class=" form-control" type="email">
                            <small class="text-danger">@error('email') {{$message}}@enderror</small>
                        </div>
                        <div class="col-md-4">
                           <label> Message : </label><textarea value="{{old('message')}}" name="message" class=" form-control" ></textarea>
                            <small class="text-danger">@error('message') {{$message}}@enderror</small>
                        </div>
                        <div class="col-md-4 mt-2">
                            <button type="submit" class="btn btn-info">Send</button>
                        </div>
                    </form>
                </div>
            </section>
        </section>
        <!-- /.content -->
    </div>
@endsection
