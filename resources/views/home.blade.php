
@extends('layouts.master.master')

@section('content')
    <div class="content-wrapper" style="min-height: 511px;">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <h1>Laravel Test Home Page</h1>
                </div>
            </div>
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        <section>
            <section class="content">
                <div class="container-fluid" id="app">
                    <div class="row">
                        <div class="col-6">
                            <h3>What is Lorem Ipsum?
                            </h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                        <div class="col-6">
                            <h3>What is Lorem Ipsum?
                            </h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                        <div class="col-6">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="https://fakeimg.pl/350x200/ff0000/000">

                    </div>
                </div>
            </section>
        </section>
        <!-- /.content -->
    </div>
@endsection
