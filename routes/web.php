<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , 'MainController@index')->name('home');
Route::get('/contact' , 'MainController@contact')->name('contact');
Route::get('/admin/list' , 'MainController@list')->name('list');

Route::post('/add' , 'MainController@store')->name('store.message');
