<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessageRequest;
use App\Message;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
       return view('home');
    }

    public function contact()
    {
        return view('contact');
    }
    public function list()
    {
        $messages = Message::all();
        return view('admin.list',compact(['messages']));
    }

    public function store(StoreMessageRequest $request)
    {

        $inputs = $request->only(['name','email','message']);

        $message = new Message();
        $message->name = $inputs['name'];
        $message->email = $inputs['email'];
        $message->message = $inputs['message'];

        $message->save();

        return redirect('/admin/list')->with('message', 'Successfully Store Message');
    }

}
